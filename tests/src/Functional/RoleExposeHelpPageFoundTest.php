<?php

namespace Drupal\Tests\role_expose\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Make sure help page text populates and is readable only with proper perms.
 *
 * @group role_expose
 */
class RoleExposeHelpPageFoundTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['role_expose', 'help'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Make sure admin can read help text.
   */
  public function testAdminUserCanReadHelp() {

    // Create a user and login.
    $account = $this->drupalCreateUser(['access administration pages']);
    $this->drupalLogin($account);

    // Verify Role Expose is listed in help pages.
    $this->drupalGet('admin/help');
    $this->assertLink('Role Expose', 0, 'Check Help page has module name as link.', 'Role Expose');
    // Verify Role Expose page has correct content.
    $this->clickLink('Role Expose');
    $this->assertText('Role Expose -module gives site administrators ability to expose user their own user roles.', 'Check Help page has module help test (check beginning of text).');
  }

}
