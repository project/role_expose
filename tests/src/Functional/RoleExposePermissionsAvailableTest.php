<?php

namespace Drupal\Tests\role_expose\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Make sure all grant's are available in <em>admin/config/people</em>.
 *
 * @group role_expose
 */
class RoleExposePermissionsAvailableTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['role_expose'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test that permissions are in place in permission granting page.
   */
  public function testRoleExposeUserPermissionsGrantable() {

    $user_admin = $this->drupalCreateUser(['administer permissions']);
    $this->drupalLogin($user_admin);

    $this->drupalGet('admin/people/permissions');

    $this->assertText('View own exposed roles', '"View own exposed roles" -grant available');

    $warning = 'Warning: Give to trusted roles only; this permission has security implications.';
    $perms_2 = 'View exposed roles for all users';
    $this->assertText($perms_2, '"View exposed roles for all users" -grant available');
    $this->assertRaw('<div class="permission"><span class="title">' . $perms_2
        . '</span><div class="description"><em class="permission-warning">'
        . $warning, '"restrict access" effective with "View exposed roles for all users" -permission');
  }

}
