<?php

namespace Drupal\Tests\role_expose\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Make sure content exists on logged in user profile page.
 *
 * @group role_expose
 */
class RoleExposeRoleFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['role_expose'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Make sure all user roles are listed in config page.
   */
  public function testUserRoleForm() {

    $user_admin = $this->drupalCreateUser(['administer permissions']);
    $this->drupalLogin($user_admin);

    $this->drupalGet('admin/people/roles/add');
    // Check administrator -role, by form element name.
    $this->assertText('Role expose', 'Selector label exists.');
    $this->assertOption('edit-role-expose', '0', 'Select option "Never" is present.');
    $this->assertOption('edit-role-expose', '1', 'Select option "User with this role" is present.');
    $this->assertOption('edit-role-expose', '2', 'Select option "User without this role" is present.');
    $this->assertOption('edit-role-expose', '3', 'Select option "Always" is present.');
    $this->assertText('Choose when this role should displayed in User profile page.', 'Selector Description text found.');
  }

}
